﻿namespace files_merge
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gridFileInfo = new System.Windows.Forms.DataGridView();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EndAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilePath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FileComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpFiles = new System.Windows.Forms.GroupBox();
            this.chkHexFormat = new System.Windows.Forms.CheckBox();
            this.menuMainPage = new System.Windows.Forms.MenuStrip();
            this.menuFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenCfgFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveCfgFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuAddFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAddFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDeleteFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuClearFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMergeFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFilesInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDefaultPadByte = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPadByteCombox = new System.Windows.Forms.ToolStripComboBox();
            this.menuAbort = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSoftInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSoftHelp = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridFileInfo)).BeginInit();
            this.grpFiles.SuspendLayout();
            this.menuMainPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridFileInfo
            // 
            this.gridFileInfo.AllowUserToAddRows = false;
            this.gridFileInfo.AllowUserToDeleteRows = false;
            this.gridFileInfo.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Honeydew;
            this.gridFileInfo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.gridFileInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridFileInfo.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.gridFileInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFileInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.gridFileInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridFileInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Address,
            this.FileLength,
            this.EndAddress,
            this.FilePath,
            this.FileComment});
            this.gridFileInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.gridFileInfo.Location = new System.Drawing.Point(9, 20);
            this.gridFileInfo.Name = "gridFileInfo";
            this.gridFileInfo.RowHeadersWidth = 32;
            this.gridFileInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridFileInfo.RowTemplate.Height = 23;
            this.gridFileInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridFileInfo.Size = new System.Drawing.Size(786, 459);
            this.gridFileInfo.TabIndex = 19;
            this.gridFileInfo.TabStop = false;
            this.gridFileInfo.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gridFileInfo_RowsChanged);
            this.gridFileInfo.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.gridFileInfo_RowsChanged);
            this.gridFileInfo.SelectionChanged += new System.EventHandler(this.gridFileInfo_SelectionChanged);
            // 
            // Address
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Format = "D1";
            dataGridViewCellStyle9.NullValue = "0";
            this.Address.DefaultCellStyle = dataGridViewCellStyle9;
            this.Address.HeaderText = "起始地址";
            this.Address.MaxInputLength = 8;
            this.Address.MinimumWidth = 64;
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            this.Address.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Address.Width = 64;
            // 
            // FileLength
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.Format = "N0";
            dataGridViewCellStyle10.NullValue = "0";
            this.FileLength.DefaultCellStyle = dataGridViewCellStyle10;
            this.FileLength.HeaderText = "文件长度";
            this.FileLength.MinimumWidth = 64;
            this.FileLength.Name = "FileLength";
            this.FileLength.ReadOnly = true;
            this.FileLength.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FileLength.Width = 64;
            // 
            // EndAddress
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.NullValue = "0";
            this.EndAddress.DefaultCellStyle = dataGridViewCellStyle11;
            this.EndAddress.HeaderText = "结束地址";
            this.EndAddress.MaxInputLength = 10;
            this.EndAddress.MinimumWidth = 64;
            this.EndAddress.Name = "EndAddress";
            this.EndAddress.ReadOnly = true;
            this.EndAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EndAddress.Width = 64;
            // 
            // FilePath
            // 
            this.FilePath.HeaderText = "文件路径";
            this.FilePath.MinimumWidth = 260;
            this.FilePath.Name = "FilePath";
            this.FilePath.ReadOnly = true;
            this.FilePath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FilePath.Width = 260;
            // 
            // FileComment
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.FileComment.DefaultCellStyle = dataGridViewCellStyle12;
            this.FileComment.HeaderText = "文件描述";
            this.FileComment.MinimumWidth = 300;
            this.FileComment.Name = "FileComment";
            this.FileComment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FileComment.Width = 300;
            // 
            // grpFiles
            // 
            this.grpFiles.Controls.Add(this.chkHexFormat);
            this.grpFiles.Controls.Add(this.gridFileInfo);
            this.grpFiles.Location = new System.Drawing.Point(12, 28);
            this.grpFiles.Name = "grpFiles";
            this.grpFiles.Size = new System.Drawing.Size(803, 507);
            this.grpFiles.TabIndex = 36;
            this.grpFiles.TabStop = false;
            this.grpFiles.Text = "文件列表";
            // 
            // chkHexFormat
            // 
            this.chkHexFormat.AutoSize = true;
            this.chkHexFormat.Location = new System.Drawing.Point(6, 485);
            this.chkHexFormat.Name = "chkHexFormat";
            this.chkHexFormat.Size = new System.Drawing.Size(198, 16);
            this.chkHexFormat.TabIndex = 31;
            this.chkHexFormat.Text = "地址和长度数据采用HEX格式显示";
            this.chkHexFormat.UseVisualStyleBackColor = true;
            this.chkHexFormat.CheckedChanged += new System.EventHandler(this.chkHexFormat_CheckedChanged);
            // 
            // menuMainPage
            // 
            this.menuMainPage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFiles,
            this.menuSetup,
            this.menuAbort});
            this.menuMainPage.Location = new System.Drawing.Point(0, 0);
            this.menuMainPage.Name = "menuMainPage";
            this.menuMainPage.Size = new System.Drawing.Size(827, 25);
            this.menuMainPage.TabIndex = 38;
            this.menuMainPage.Text = "menuStrip1";
            // 
            // menuFiles
            // 
            this.menuFiles.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOpenCfgFile,
            this.menuSaveCfgFile,
            this.toolStripSeparator1,
            this.menuAddFile,
            this.menuAddFiles,
            this.menuDeleteFile,
            this.menuClearFile,
            this.menuMergeFile,
            this.toolStripSeparator2,
            this.menuFilesInfo});
            this.menuFiles.Name = "menuFiles";
            this.menuFiles.Size = new System.Drawing.Size(44, 21);
            this.menuFiles.Text = "文件";
            // 
            // menuOpenCfgFile
            // 
            this.menuOpenCfgFile.Name = "menuOpenCfgFile";
            this.menuOpenCfgFile.Size = new System.Drawing.Size(160, 22);
            this.menuOpenCfgFile.Text = "从配置文件导入";
            this.menuOpenCfgFile.Click += new System.EventHandler(this.menuOpenCfgFile_Click);
            // 
            // menuSaveCfgFile
            // 
            this.menuSaveCfgFile.Name = "menuSaveCfgFile";
            this.menuSaveCfgFile.Size = new System.Drawing.Size(160, 22);
            this.menuSaveCfgFile.Text = "保存为配置文件";
            this.menuSaveCfgFile.Click += new System.EventHandler(this.menuSaveCfgFile_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            // 
            // menuAddFile
            // 
            this.menuAddFile.Name = "menuAddFile";
            this.menuAddFile.Size = new System.Drawing.Size(160, 22);
            this.menuAddFile.Text = "添加单个文件";
            this.menuAddFile.Click += new System.EventHandler(this.menuAddFile_Click);
            // 
            // menuAddFiles
            // 
            this.menuAddFiles.Name = "menuAddFiles";
            this.menuAddFiles.Size = new System.Drawing.Size(160, 22);
            this.menuAddFiles.Text = "添加多个文件";
            this.menuAddFiles.Click += new System.EventHandler(this.menuAddFiles_Click);
            // 
            // menuDeleteFile
            // 
            this.menuDeleteFile.Name = "menuDeleteFile";
            this.menuDeleteFile.Size = new System.Drawing.Size(160, 22);
            this.menuDeleteFile.Text = "删除选中文件";
            this.menuDeleteFile.Click += new System.EventHandler(this.menuDeleteFile_Click);
            // 
            // menuClearFile
            // 
            this.menuClearFile.Name = "menuClearFile";
            this.menuClearFile.Size = new System.Drawing.Size(160, 22);
            this.menuClearFile.Text = "清空文件列表";
            this.menuClearFile.Click += new System.EventHandler(this.menuDeleteAll_Click);
            // 
            // menuMergeFile
            // 
            this.menuMergeFile.Name = "menuMergeFile";
            this.menuMergeFile.Size = new System.Drawing.Size(160, 22);
            this.menuMergeFile.Text = "合并文件";
            this.menuMergeFile.Click += new System.EventHandler(this.menuMerge_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(157, 6);
            // 
            // menuFilesInfo
            // 
            this.menuFilesInfo.Name = "menuFilesInfo";
            this.menuFilesInfo.Size = new System.Drawing.Size(160, 22);
            this.menuFilesInfo.Text = "输出文件信息表";
            this.menuFilesInfo.Click += new System.EventHandler(this.menuFilesInfo_Click);
            // 
            // menuSetup
            // 
            this.menuSetup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuDefaultPadByte});
            this.menuSetup.Name = "menuSetup";
            this.menuSetup.Size = new System.Drawing.Size(44, 21);
            this.menuSetup.Text = "设置";
            // 
            // menuDefaultPadByte
            // 
            this.menuDefaultPadByte.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuPadByteCombox});
            this.menuDefaultPadByte.Name = "menuDefaultPadByte";
            this.menuDefaultPadByte.Size = new System.Drawing.Size(180, 22);
            this.menuDefaultPadByte.Text = "空位填充字节...";
            // 
            // menuPadByteCombox
            // 
            this.menuPadByteCombox.MaxDropDownItems = 10;
            this.menuPadByteCombox.Name = "menuPadByteCombox";
            this.menuPadByteCombox.Size = new System.Drawing.Size(75, 25);
            // 
            // menuAbort
            // 
            this.menuAbort.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSoftInfo,
            this.menuSoftHelp});
            this.menuAbort.Name = "menuAbort";
            this.menuAbort.Size = new System.Drawing.Size(44, 21);
            this.menuAbort.Text = "关于";
            // 
            // menuSoftInfo
            // 
            this.menuSoftInfo.Name = "menuSoftInfo";
            this.menuSoftInfo.Size = new System.Drawing.Size(124, 22);
            this.menuSoftInfo.Text = "软件信息";
            // 
            // menuSoftHelp
            // 
            this.menuSoftHelp.Name = "menuSoftHelp";
            this.menuSoftHelp.Size = new System.Drawing.Size(124, 22);
            this.menuSoftHelp.Text = "帮助";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 547);
            this.Controls.Add(this.menuMainPage);
            this.Controls.Add(this.grpFiles);
            this.MainMenuStrip = this.menuMainPage;
            this.Name = "MainForm";
            this.Text = "多功能文件合并";
            ((System.ComponentModel.ISupportInitialize)(this.gridFileInfo)).EndInit();
            this.grpFiles.ResumeLayout(false);
            this.grpFiles.PerformLayout();
            this.menuMainPage.ResumeLayout(false);
            this.menuMainPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView gridFileInfo;
        private System.Windows.Forms.GroupBox grpFiles;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn EndAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilePath;
        private System.Windows.Forms.DataGridViewTextBoxColumn FileComment;
        private System.Windows.Forms.CheckBox chkHexFormat;
        private System.Windows.Forms.MenuStrip menuMainPage;
        private System.Windows.Forms.ToolStripMenuItem menuFiles;
        private System.Windows.Forms.ToolStripMenuItem menuOpenCfgFile;
        private System.Windows.Forms.ToolStripMenuItem menuSaveCfgFile;
        private System.Windows.Forms.ToolStripMenuItem menuSetup;
        private System.Windows.Forms.ToolStripMenuItem menuDefaultPadByte;
        private System.Windows.Forms.ToolStripMenuItem menuAbort;
        private System.Windows.Forms.ToolStripMenuItem menuSoftInfo;
        private System.Windows.Forms.ToolStripMenuItem menuSoftHelp;
        private System.Windows.Forms.ToolStripComboBox menuPadByteCombox;
        private System.Windows.Forms.ToolStripMenuItem menuAddFile;
        private System.Windows.Forms.ToolStripMenuItem menuAddFiles;
        private System.Windows.Forms.ToolStripMenuItem menuDeleteFile;
        private System.Windows.Forms.ToolStripMenuItem menuClearFile;
        private System.Windows.Forms.ToolStripMenuItem menuMergeFile;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menuFilesInfo;
    }
}

